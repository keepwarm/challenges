// --- Directions
// Given a string, return the character that is most
// commonly used in the string.
// --- Examples
// maxChar("abcccccccd") === "c"
// maxChar("apple 1231111") === "1"


function maxChar(str) {
    //turn to object and count values
    const obj = {}
    for(let char of str){
       obj[char] = obj[char] + 1 || 1;
    }
    //turn keys of object to array: 
    const keys = Object.keys(obj);
    let currentHighestVal = 0; 
    let currentCharHighest = '';
    for(let key of keys){
        if(obj[key] > currentHighestVal) {
            currentHighestVal = obj[key];
            currentCharHighest = key;
        } 
    }
    return currentCharHighest;
}

function maxCharSolution1(str){
    const charMap = {};
    let max = 0;
    let maxChar = '';

    for(let char of str){
        if(charMap[char]) {
            charMap[char]++;
        } else {
            charMap[char] = 1; 
        }
    }

    console.log(charMap);

    for(let char in charMap){
        if(charMap[char] > max){
            max = charMap[char];
            maxChar = char;
        }
    }
    return maxChar;
}

module.exports = maxChar;


// const test = {
//     firstName: 'hien',
//     lastName: 'ho'
// }

// Objext.keys(test);
// ['firstName', 'lastName']
// Object.values(test);
// ['hien', 'ho']



























// const obj = {};
//     for(let char of str){
//         if(!obj[char]){
//             obj[char] = 1;
//         }
//         obj[char] = obj[char] + 1;
//     }
//     const keys = Object.keys(obj); //keys are the propertity/key of the object 
// //  const values = Object.values(obj);
//     console.log('keys: ' + keys);
//     let currentHighestValue = 0;
//     let currentHighestKey = '';
//     for(let key of keys){
//         if(obj[key] > currentHighestValue){
//             currentHighestValue = obj[key];
//             currentHighestKey = key;
//         }
//     }
//     return currentHighestKey;