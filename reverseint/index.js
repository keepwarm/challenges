// --- Directions
// Given an integer, return an integer that is the reverse
// ordering of numbers.
// --- Examples
//   reverseInt(15) === 51
//   reverseInt(981) === 189
//   reverseInt(500) === 5
//   reverseInt(-15) === -51
//   reverseInt(-90) === -9

function reverseInt(n) {
        console.log('num: ' + n);
    const reverseToString = n.toString().split('').reverse().join('');
        console.log('string:' + reverseToString);
    const changetoInt = parseInt(reverseToString);
        console.log('integer: ' + changetoInt);
        console.log('sign: ' + Math.sign(changetoInt));
    const final = Math.sign(n) * changetoInt;
        console.log(final);
    return final
}

function reverseIntSolution1(n){
    const reversed = n.toString().split('').reverse().join('');
    if ( n < 0){
        return parseInt(reserved) * -1;
    }
    return parseInt(reversed);
}

function reverseIntSolution2(n){
    const reversed = n.toString().split('').reverse().join('');
    return parseInt(reversed) * Math.sign(n);
}


module.exports = reverseInt;
