// --- Directions
// Given a string, return a new string with the reversed
// order of characters
// --- Examples
//   reverse('apple') === 'leppa'
//   reverse('hello') === 'olleh'
//   reverse('Greetings!') === '!sgniteerG'

// function reverse(str) {
//     let reverseString = '';
//     for(let i = str.length - 1; i >= 0; i--){
//         console.log('i=' + i);
//         reverseString = reverseString + str[i];
//         console.log('reverseString = ' + reverseString);
//     }
//     return reverseString;
// }

// function reverseSolution1(str){
//     const arr = str.split('');
//     arr.reverse();
//     return arr.join('');
//     // return str.split('').reverse().join('');
// }

// function reverseSolution2(str){
//     let reversed = '';
//     for (let character of str){
//         reversed = character + reversed;
//     }
//     return reversed;
// }

// function reverseSolution3(str){
//     return str.split('').reduce((reversed, character) => {
//         return character + reversed;
//     }, '');
// }
// //===
// function reverseSolution3(str){
//     return str.split('').reduce((rev, char) => char + rev, '');
// }

function reverse(str){
    debugger;
    return str.split('').reduce((rev, char) => char + rev, '');
}

reverse('asdf');

module.exports = reverse;

