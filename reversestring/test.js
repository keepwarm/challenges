const reverse = require('./index');

test('Reverse function exists', () => {
  expect(reverse).toBeDefined();
  // expect(reverse).not.toEqual(undefined);
});

test('Reverse reverses a string', () => {
  expect(reverse('abcd')).toEqual('dcba');
});

test('Reverse reverses a string', () => {
  expect(reverse('  abcd')).toEqual('dcba  ');
});
